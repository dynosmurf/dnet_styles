
/* 
 * This will be a command line script fro upgrading a theme file. 
 *
 *
 */


var fs = require('fs');
var diff = require('diff');


upgrade = exports.upgrade = function(default_file, target_file){

    var target_vars = get_vars(target_file); 
    var default_str = default_file.join('\n')+'\n';
    var target_str = target_file.join('\n')+'\n';
    var patch = diff.createPatch('test', target_str, default_str);



    var upgraded = diff.applyPatch(target_str, patch);

    var out = modify_lines(target_vars, upgraded.split('\n'));

    return out;

}

exports.get_changes = function(default_file, target_file){
    var old_file = target_file;
    var new_file = upgrade(default_file, target_file);

    var missing = missing_vars(default_file, target_file);
    var extra = extra_vars(default_file, target_file);
    var missing_out = [];
    var extra_out = [];
    var out = [];

    for(var i = 0; i < extra.length; i++){
        var line = get_line_for_var(extra[i], old_file);
        extra_out.push(line.number+": "+line.text);
    }

    for(var i = 0; i < missing.length; i++){
        var line = get_line_for_var(missing[i], new_file);
        missing_out.push(line.number+": "+line.text);
    }
    if(extra.length){
        out.push(["REMOVED: -----------------------"]);
        out = out.concat(extra_out);
    }

    if(missing.length){
        out.push(["ADDED: +++++++++++++++++++++++++"]);
        out = out.concat(missing_out);
    }

    return out.join('\n');

}

get_line_for_var = exports.get_line_for_var = function(var_name, file){
    var re = /^\s*@([A-Za-z0-9-]+):(.*);\s*(\/.+\/)?\s*$/;
    var output = [];
    for(var i = 0; i < file.length; i++){
        var line = file[i];
        var match = line.match(re);
        if(match && match[1] == var_name){
            return {number: i, text: line};
        }
    }
    return false;
}



get_vars = exports.get_vars = function(data){
    var re = /^\s*@([A-Za-z0-9-]+)\s*:(.*);\s*(\/.+\/)?\s*$/;
    var output = [];
    for(var i = 0; i < data.length; i++){
        var line = data[i];
        var match = line.match(re);
        if(match && match.length){
            output.push({
                'key':match[1],
                'value':match[2] || '',
                'comment': match[3] || ''
            });
        }
    }
    return output;
}

missing_vars = exports.missing_vars = function(default_data, theme_data){
    var default_vars = get_vars(default_data);
    var theme_vars = get_vars(theme_data);
    var missing = [];

    for(var i = 0; i < default_vars.length; i++){
        var key = default_vars[i].key;
        var match_found = false;

        for(var j = 0; j < theme_vars.length; j++){
            if(theme_vars[j].key == key){
                match_found = true;
                break;
            }
        }
        if(!match_found){

            missing.push(key);
        }
    }

    return missing;
}

extra_vars = exports.extra_vars = function(default_data, theme_data){ 
    var default_vars = get_vars(default_data);
    var theme_vars = get_vars(theme_data);
    var extra = [];

    for(var i = 0; i < theme_vars.length; i++){
        var key = theme_vars[i].key;
        var match_found = false;

        for(var j = 0; j < default_vars.length; j++){
            if(default_vars[j].key == key){
                match_found = true;
                break;
            }
        }
        if(!match_found){

            extra.push(key);
        }
    }

    return extra;
}

exports.duplicate_vars = function(data){ 
    var vars = get_vars(data);
    var tested = [];
    var duplicates = [];
    var extra = [];

    for(var i = 0; i < vars.length; i++){
        var key = vars[i].key;
        var duplicate_found = false;

        for(var j = 0; j < tested.length; j++){
            if(tested[j] == key){
                duplicate_found = true;
                break;
            }
        }
        if(duplicate_found){
            duplicates.push(key);
        } else {
            tested.push(key);
        }
    }

    return duplicates;
}


modify_lines = exports.modify_lines = function(data, file){
    // step through lines 
    // parse line and get var
    // if var in data, modify the line
    //

    var out_lines = [];
    var re = /(^\s*@[A-Za-z0-9-]+:)(.*)(;\s*(\/.+\/)?\s*$)/;
    for(var i = 0; i < file.length; i++){
        var line_vars = get_vars([file[i]]);
        var line = file[i];
        var new_line = line;

        for(j = 0; j < data.length; j++){
            if(line_vars.length > 0 && data[j].key == line_vars[0].key){
                new_line = line.replace(re, "$1"+ data[j].value +"$3");
            }
        }

        out_lines.push(new_line);
    }

    return out_lines;
}


