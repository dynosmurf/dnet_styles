var less = require('less');
var os = require('os');
var fs = require('fs');
var rimraf = require('rimraf');
var CleanCSS = require('clean-css');
var async = require('async');

var get_child_dirs = function(parent) {
  return fs.readdirSync(parent).filter(function(file) {
    return fs.statSync(path.join(parent, file)).isDirectory();
  });
}

var copyFile = function(source, target, cb) {
    var cbCalled = false;

    var rd = fs.createReadStream(source);
    rd.on("error", function(err) {
        done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function(err) {
        done(err);
    });
    wr.on("close", function(ex) {
        done();
    });
    rd.pipe(wr);

    function done(err) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }
}

create_temp_structure = exports.create_temp_structure = function(core_path, pub_path, callback){
    var tmp = os.tmpdir();
    tmp = tmp.replace(/\/?$/, '/'); // add trailing slash if not present
    var ncp = require('ncp').ncp;
    var theme_path = pub_path + "theme.less";
    var custom_path = pub_path + "custom.css";
    var temp_styles = tmp+'mobile_styles/mobile/';
    var core_styles = core_path + "public_html/styles/mobile/";

    // filter any theme files that might be hanging out in core, we only
    // want the theme from the pub to be imported. 
    var filter = function(name){
        if(name.match(/[/]theme.less/)){
            return false;
        } 
        return true;
    }

     async.waterfall([
         //remove old temp dir
         rimraf.bind(null, tmp+'mobile_styles/'),
         // make new dir structure
         fs.mkdir.bind(null, tmp+'mobile_styles/'),
         fs.mkdir.bind(null, tmp+'mobile_styles/mobile/'),
         // copy in core files
         ncp.bind(null, core_styles, temp_styles, {filter:filter}),
         // copy in pub files
         copyFile.bind(null, theme_path, temp_styles+'/theme.less'),
         copyFile.bind(null, custom_path, temp_styles+'/custom.css')
     ], function(err, result){
         if (err) {
             callback(err);
         } else {
             callback(null, temp_styles);
         }
     });
}

exports.generate_css = function(temp_path, callback){
    var build_path = temp_path+'less/build.less';
    var less_path = temp_path+'less/';

    var options = {
        paths: [
            less_path,
            temp_path
        ],  
        filename: 'build.less', 
    }

    var less_wrapper = function(data, callback){
        less.render(data, options, function(err, result){
            callback(err, result);
        });
    }

    async.waterfall([
        // read build file
        fs.readFile.bind(null, build_path, {encoding: 'utf8'}),
        // render less with options
        less_wrapper
    ], function(err, result){
         if (err) {
             callback(err);
         } else {
             callback(null, result);
         }
    });

}

minify_css = exports.minify_css = function(css){
    var minified = new CleanCSS({
        debug:true,
    }).minify(css);
    return minified
}

exports.file_writer = function(admin_dest, public_dest, less_output, callback){
    var css = less_output.css;
    var minified_css = minify_css(less_output.css).styles; 

    var inner_callback =   function(err, results){
        callback(null, "FILES SAVED");
    }
    // write files, if public or admin is null skip them. 
    if(public_dest && admin_dest){
        async.parallel([
            fs.writeFile.bind(null, admin_dest, css), 
            fs.writeFile.bind(null, public_dest, minified_css)
        ], inner_callback );
    } else if (admin){
        fs.writeFile(null, admin_dest, css, inner_callback );
    }
}
