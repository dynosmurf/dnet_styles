###################
Mobile CSS Build Script v 0.0.3 README
###################

This is a temporary solution to build the styles for a mobile site. To use you must
have the following setup on your system. 

Node, npm

----------------------------
 Installation
----------------------------

Here is a step by step guide to getting the needed tools installed. 

1. make sure you have node installed. 

    see: http://nodejs.org/download/

    or if you use brew: 
        brew install node

2. Make sure you have npm (node package manager)

    run in terminal:
        npm

    You should see help output on the command line. 

3. Run
    
    npm install -g git+http://bitbucket.org/dynosmurf/dnet_styles.git 

    note you may need to use sudo with this command depending on your node install.

4. Before you can make use of the build script you must set the location of the 
   relevant repositories on your system. 

   To do this you must run three commands. 

   1: path to core
   -----------------------
   
       ev_styles config core <path to the core repo>

   2:  path to devel
   -----------------------

       ev_styles config dev <path to the core repo>

   This command may be different if you are testing on a dev branch. For example if
   I am going to use the "ryan" dev instance to test styles I would instead run:

       ev_styles config dev ryan 

   However, since devel is backed by the core repository in most cases this will be 
   the correct one to use. 

   3: path to publication working copies
   ---------------------------------------------

       ev_styles config pubs <path to publication directory>

   The script assumes you keep all of the publication working copies in the same 
   directory. For example

    ~/publication_dir
        /thecoast
        /tucsonweekly
        /bend
        ...

   And in this case you would run. 

       ev_styles config pubs ~/publication_dir


5. Run 

       ev_styles config 

   to check that the configuration is correct.


----------------------------
 Setup a new client:
----------------------------

1. Update Core.

2. Update client working copy. 

3. Run:
    ev_styles setup <pubname>

4. Modify the theme.less file to match their desktop site. Walk through each
    variable definition and confirm it makes sense for that client. 

5. Any additional needs or style embellishments can be added to custom.css. Try
    to minimise the extent of changes to custom.css. If a similar change is needed
    on every client. We can make it a default style option in the theme.less file. 

6. Test styles on devel. run:

    ev_styles dev <pubname>

    then commit the following files in the core working copy. 
        /core/public_html/styles/mobile/mobile.css
        /core/public_html/styles/mobile/mobile.min.css

    test on url (assuming you are using core as your dev config location)
        http://a.devel.foundation.gyrobase.com/

----------------------------
 Building On a pub 
----------------------------

1. Update both core and publication working copies. 

2. Run ev_styles build <pubname>

3. Commit 

        /<pubname>/public_html/styles/mobile/mobile.css
        /<pubname>/public_html/styles/mobile/mobile.min.css

4. Test thouroghly to make sure the build was succesful. 

5. If you are making a change that could have significant impact, you can instead
   use the command:

       ev_styles build -a <pubname>

   This will only build the admin version of the styles.


----------------------------
 Theme Managmement:
----------------------------

If the theme file for a client is missing any of the variables required the build 
will throw an error. For this reason this tool includes a command to check a theme
file against the default theme file in the core directory. 

To see the status of all theme files in your pubs directory run:
    
    ev_styles theme -s

  ### Output ###

    styleweekly ...................✖
      + new-default-var
    tampa .........................✖
      + new-default-var
    thecoast ......................✖
      + new-default-var
      - test

This indicates that all three pubs are missing a variable that is present in the 
default core styles. Additionally "thecoast" has an additional variable that is not 
defined in the default styles. 

To upgrade a theme file to be in sync with the default you must run:

    ev_styles theme -u -p tampa

  ### Output ###

    Changes to tampa theme file:
    .../tampa/public_html/styles/mobile/theme.less

    ADDED: +++++++++++++++++++++++++
      48: @new-default-var:                           test;

Running the status command again now shows that "tampa" is in sync with defaults.

    ev_styles theme -u -p tampa

  ### Output ###

    styleweekly ...................✖
      + new-default-var
    tampa .......................✓
    thecoast ......................✖
      + new-default-var
      - test

Doing the same for "thecoast" in this instance will also remove the extra var. 

    Changes to thecoast theme file:
    .../thecoast/public_html/styles/mobile/theme.less

    REMOVED: -----------------------
    158: @test:                                     xxx;
    ADDED: +++++++++++++++++++++++++
    48: @new-default-var:                           test;

The output of the status command will now show:

    styleweekly ...................✖
      + new-default-var
    tampa .......................✓
    thecoast ....................✓

Upgrading the theme file only copies the line from the default file. This means that
in addition to running the command in most cases it will be necessary to confirm
that the newly added variable has a reasonable definition for that pub. 





Note: it is not necessary to build the theme on the client until launch time. 

Additional Resources:

LESS - http://lesscss.org/
SEMANTIC UI - http://semantic-ui.com/