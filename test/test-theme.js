
var Script = require('vm').Script,
fs     = require('fs'),
path   = require('path'),
mod    = require('module');

var debug = false;
if( debug == true ){

    log = console.log;

} else {
    log = function(){console.log('.')};

}



app = require('../lib/theme.js');
log(app.get_vars);
app.get_vars(['testseasdf','asdfasdfa']);



exports.test_get_vars = function(test){
    var test_data, out;

    test_data = [];
    log('\nTEST DATA >>',test_data);
    
    out = app.get_vars(test_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 0, "Output should be empty");


    test_data = ['@test-var:;',''];
    log('\nTEST DATA >> ',test_data);
    out = app.get_vars(test_data);
    log('OUTPUT >> ', out);
    test.equal(out.length, 1, "Output should have one var");
    test.equal(out[0].key, 'test-var', "Output should have correct key");
    test.equal(out[0].value, '', "Output value should be empty");
    test.equal(out[0].comment, '', "Output comment should be empty");

    test_data = [
        '@test-var: 120px; /*asdfas asdfaff; */',
        '   @test-var2: (asdfasF) ;    '
    ];
    log('\nTEST DATA >> ',test_data);
    out = app.get_vars(test_data);
    log('OUTPUT >> ', out);
    test.equal(out.length, 2, "Output should have two vars");
    test.equal(out[0].key, 'test-var', "Output[0] should have correct key");
    test.equal(out[1].key, 'test-var2', "Output[1] should have correct key");
    test.equal(out[0].value, ' 120px', "Output[0] value should be correct");
    test.equal(out[1].value, ' (asdfasF) ', "Output[1] value should be correct");
    test.equal(out[0].comment, '/*asdfas asdfaff; */', "Output[0] comment should be correct");
    test.equal(out[1].comment, '', "Output[1] comment should be empty");

    test.done();
};

exports.test_missing_vars = function(test){
    var test_default_data, test_theme_data, out;

    test_default_data = [];
    test_theme_data = [];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.missing_vars(test_default_data, test_theme_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 0, "Output should be empty");

    test_default_data = [
        '@test-var:;',
        '@another-var:;'
    ];
    test_theme_data = [
        '@test-var:;'
    ];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.missing_vars(test_default_data, test_theme_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 1, "Output should have one value");
    test.equal(out[0], 'another-var', "Output should be correct");
    /* duplicate variables cause failure... not sure if desired 
       test_default_data = [
       '@test-var:;',
       '@another-var:;',
       '',
       '@another-var:;',
       '@third-var:;'
       ];
       test_theme_data = [
       '@test-var:;',
       '@extra-var:;'
       ];
       log('\nTEST DATA >>',test_default_data, test_theme_data);
       out = app.missing_vars(test_default_data, test_theme_data);
       log('OUTPUT >> ',out);
       test.equal(out.length, 2, "Output should have two values");
       test.equal(out[0], 'another-var', "Output should be correct");
       test.equal(out[1], 'third-var', "Output should be correct");
       */

    test.done();
};

exports.test_extra_vars = function(test){
    var test_default_data, test_theme_data, out;

    test_default_data = [];
    test_theme_data = [];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.extra_vars(test_default_data, test_theme_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 0, "Output should be empty");

    test_default_data = [
        '@test-var:;',
        '@another-var:;'
    ];
    test_theme_data = [
        '@test-var:;'
    ];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.extra_vars(test_default_data, test_theme_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 0, "Output should be empty");

    test_default_data = [
        '@test-var:;',
        '@another-var:;',
        '',
        '@another-var:;',
        '@third-var:;'
    ];
    test_theme_data = [
        '@test-var:;',
        '@extra-var:;'
    ];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.extra_vars(test_default_data, test_theme_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 1, "Output should have one value");
    test.equal(out[0], 'extra-var', "Output should be correct");


    test.done();
};

exports.test_duplicate_vars = function(test){
    var test_data, out;

    test_data = [];
    log('\nTEST DATA >>',test_data);
    
    out = app.duplicate_vars(test_data);
    log('OUTPUT >> ',out);
    test.equal(out.length, 0, "Output should be empty");


    test_data = ['@test-var:;','@test-var:;'];
    log('\nTEST DATA >> ',test_data);
    out = app.duplicate_vars(test_data);
    log('OUTPUT >> ', out);
    test.equal(out.length, 1, "Output should have one var");
    test.equal(out[0], 'test-var', "Output should have correct value");

    test_data = [
        '@test-var:;','@test-var:;',
        '@t-var:;','@t-var:;',
        '@ta-var:;','@tb-var:;',
    ];
    log('\nTEST DATA >> ',test_data);
    out = app.duplicate_vars(test_data);
    log('OUTPUT >> ', out);
    test.equal(out.length, 2, "Output should have two vars");
    test.equal(out[0], 'test-var', "Output[0] should have correct value");
    test.equal(out[1], 't-var', "Output[1] should have correct value");

    test.done();
};

/*
exports.test_add_missing = function(test){
    var test_default_data, test_theme_data, out, expected_out;

    test_default_data = [
        '@test-var:;',
        '',
        '@another-var:;'
    ];
    test_theme_data = [
        '@test-var:;'
    ];

    expected_out = [
        '@test-var:;',
        '@another-var:;'
    ];
    log('\nTEST DATA >>',test_default_data, test_theme_data);
    out = app.add_missing(test_default_data, test_theme_data);
    log('EXPRECED OUTPUT >> ',expected_out);
    log('OUTPUT >> ',out);
    test.equal(out.length, expected_out.length, "Output should be correct length");
    for(var i = 0; i < expected_out.length; i++){
        test.equal(out[i], expected_out[i], "Output should be correct");
    }

    test.done();
};
*/

exports.test_modify_lines = function(test){
    var test_data, test_file, out;

    test_data = [{
        key: 'test-var',
        value: ' 120px',
        comment: '/*should replce gibberish; */' 
    },{ 
        key: 'test-var2', 
        value: ' (xxxxx) ', 
        comment: '' 
    }];

    test_file = [
        '@test-var: 90px; /*asdfas asdfaff; */',
        '   @test-var2: (asdfasF) ;    '
    ];



    log(out);

    log('.....................');
    log('\nTEST DATA >>',test_data);
    log('\nTEST FILE >>',test_file);
    
    out = app.modify_lines(test_data, test_file);
    log('OUTPUT >> ',out);

    test.equal(out.length, 2, "Output should be same length");
    test.equal(out[0], '@test-var: 120px; /*asdfas asdfaff; */', "Output should have correct value");
    test.equal(out[1], '   @test-var2: (xxxxx) ;    ', "Output should have correct value");

    log('~~~~~~~~~~~~~~~~~~~~~');
    test.done();
};


exports.test_upgrade = function(test){
    var default_file, target_file;

    default_file = [
        '@body-color:                                #OOO;',
        '@subtle-color:                              #000;',
        '@link-color:                                #000;',
        '@new-var:                                 ooooo;',
        '@link-decoration:                           000000000;',
    ];

    target_file = [
        '   @body-color:                                #XXX; /* xxxxxx */',
        '@subtle-color:                              #XXX;',
        '@link-color:                                #XXX;',
        '@link-decoration:                           xxxxxxxx;',
        '@extra-var:                            -----;'
    ];

    expected_out = [ 
        '@body-color:                                #XXX;',
        '@subtle-color:                              #XXX;',
        '@link-color:                                #XXX;',
        '@new-var:                                 ooooo;',
        '@link-decoration:                           xxxxxxxx;',
        '' 
    ]; 



    log('\nTEST DATA >>',default_file);
    log('\nTEST DATA >>',target_file);
    
    out = app.upgrade(default_file, target_file);

    log('OUTPUT >> ',out);
    test.equal(out.length, expected_out.length, "Output length should match expected.");
    for(var i = 0; i < expected_out.length; i++){

        test.equal(out[i], expected_out[i], "Outputs should match expected.");
    }


    test.done();
};


