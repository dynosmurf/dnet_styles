#!/usr/bin/env node

var fs = require('fs'),
path = require('path'),
diff = require('diff'),
nconf = require('nconf');
async = require('async');

var args = require('minimist')(process.argv.slice(2));

var HOME = process.env[(process.platform == 'win32') ? 'USERPROFILE' : 'HOME'];
nconf.file({ file: HOME+'/.evconfig' });

var show_config = function(){
    console.log('-------------------------------------------------------------');
    console.log('Local Core repo location:            ',nconf.get('core_path'));
    console.log('Local Developement repo location:    ',nconf.get('dev_path'));
    console.log('Local Publication repo location:     ',nconf.get('publication_dir'));
    console.log('-------------------------------------------------------------');
}

var usage =  "\nMobile CSS Build Script v 0.0.3\n\n"+
    "options:\n"+
    "    dev <pubname>\n"+
    "        Builds pub css to dev pub specified in config.\n\n"+
    "    dev [-d]\n"+
    "        Specify the ./mobile/ directory to use explicitly.\n\n"+
    "    build [-a] <pubname> \n"+
    "        Builds css to publication for production\n"+
    "        -a  restrict build to admin styles (mobile.css)\n\n"+
    "    build [-d] <path> \n"+
    "        Specify the path to the mobile styles directory.\n\n"+
    "    setup [-s|--source <pubname>] <pubname>\n"+
    "        Use to add default theme.less and custom.css to publiation\n"+
    "        working copy for new site setup. \n"+
    "        -s|--source  Specify a source pub instead of core.\n\n"+
    "    setup [-d] <path> \n"+
    "        Specify path to mobile folder to copy defaults into.\n\n"+
    "    config core|dev|pubs <path>\n"+
    "        Modify the location this script uses to build styles\n\n"+
    "    theme -s|--status [-p <pub>]\n"+
    "        Output the status of the theme.less files for one or all pubs.\n\n"+
    "    theme -u|--upgrade -p <pub>\n"+
    "        Upgrade the theme.less file to the default version in core\n";

var first_arg =function(key){
    if(args._[0] && args._[0] == key){
        return true;
    } 
    return false;
}

var is_dev = first_arg("dev");
var is_build = first_arg("build");
var is_setup =first_arg("setup"); 
var is_config =first_arg("config"); 
var is_theme =first_arg("theme");

var is_status = function(){
    if(args.status || args.s){
        return true;
    }
    return false;
}();
var is_upgrade = function(){
    if(args.upgrade || args.u){
        return true;
    }
    return false;
}();

var publication_dir = nconf.get('publication_dir');
var core_path = nconf.get('core_path');
var dev_path = nconf.get('dev_path');
var mobile_dir = "public_html/styles/mobile/";


var copyFile = function(source, target, cb) {
    var cbCalled = false;

    var rd = fs.createReadStream(source);
    rd.on("error", function(err) {
        done(err);
    });
    var wr = fs.createWriteStream(target);
    wr.on("error", function(err) {
        done(err);
    });
    wr.on("close", function(ex) {
        done();
    });
    rd.pipe(wr);

    function done(err) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }
}

var get_child_dirs = function(parent) {
  return fs.readdirSync(parent).filter(function(file) {
    return fs.statSync(path.join(parent, file)).isDirectory();
  });
}

var validate_pubname = function(test_name){
    var pubs = get_child_dirs(publication_dir);   
    for(var i = 0; i < pubs.length; i++){
        if(pubs[i] == test_name){
            return true;
        }
    }
    return false;
}

if(args.h){
    console.log(usage);
    process.exit();
}

if(is_config){
    if(args.s){
        show_config();
        process.exit();
    }
    var config_key_raw = args._[1];
    var config_value_raw = args._[2];

    //validation
    if(!config_key_raw){
        show_config();
        process.exit();
    }
    if(['dev','core','pubs'].indexOf(config_key_raw) < 0){
        console.log('\nERROR: incorrect config key: '+ config_key_raw +'\n');
        process.exit();
    }
    if(!config_value_raw){
        console.log('\nERROR: must specify config directory.\n');
        process.exit();
    }

    var key_map = {
        dev : 'dev_path',
        core : 'core_path',
        pubs : 'publication_dir'
    }
    var config_key = key_map[config_key_raw];
    var config_value = (config_value_raw+'/').replace('//','/');

    if(config_key && config_value){
        nconf.set(config_key, config_value);
        nconf.save(function (err) {
            if(!err){
                console.log('\nConfiguration Saved.\n');    
                show_config();
            } else {
                console.log(err);
                show_config();
            }
        });
    } else {
        show_config();
    }
} else {

    if(typeof core_path == "undefined"){
        console.log("\nERROR: Must set 'core' config directory. (Use: ev_styles config core <dir>)\n");
        show_config();
        process.exit();
    } else if (typeof dev_path == "undefined"){
        console.log("\nERROR: Must set 'dev' config directory. (Use: ev_styles config dev <dir>)\n");
        show_config();
        process.exit();
    } else if (typeof publication_dir == "undefined"){
        console.log("\nERROR: Must set 'pubs' config directory. (Use: ev_styles config pubs <dir>)\n");
        show_config();
        process.exit();
    }

}



/*
 * DEV
 */
if(is_dev){
    var custom_path = args.d || false;
    var pub_name = args._[1];

    // validate 
    if(!custom_path){
        if(typeof pub_name == "undefined"){
            console.log('\nERROR: Must specify publication.\n');
            process.exit();
        }
        if(!validate_pubname(pub_name)){
            console.log('\nERROR: Pub "'+pub_name+'" not found.\nin: '+publication_dir+'\n');
            process.exit();
        }
    }

    var min = require('../lib/minify');

    //example: /Users/ryan/Work/Projects/diff/thecoast/public_html/styles/mobile/mobile.css
    var admin_dest = dev_path + mobile_dir + "mobile.css";
    var public_dest = dev_path + mobile_dir + "mobile.min.css";

    if(!custom_path){

        var pub_path =  publication_dir + pub_name + '/' + mobile_dir;
    } else {
        var pub_path =  custom_path;
    }


    async.waterfall([
        min.create_temp_structure.bind(null,core_path, pub_path),
        min.generate_css,
        min.file_writer.bind(null, admin_dest, public_dest)
    ], function (err, result) {
        if(!err){
            console.log('\nStyles from '+pub_name+' loaded to dev:');
            console.log([admin_dest,public_dest].join('\n'));
        } else {
            if(err.line && err.message && err.filename){
                console.log('Less Build Error ..............\n');
                console.log('  '+err.line+': '+err.message);
                console.log('in: '+err.filename);
            } else {
                console.log('ERROR: '+err);
            }
        }
        console.log('\n');
    });

}

if(is_build){
    var build_path = args.d || false;
    var pub_name = args._[1];
    var admin_only = args.a || false;

    // validate 
    if(!build_path){
        if(typeof pub_name == "undefined"){
            console.log('\nERROR: Must specify publication.\n');
            process.exit();
        }
        if(!validate_pubname(pub_name)){
            console.log('\nERROR: Pub "'+pub_name+'" not found.\nin: '+publication_dir+'\n');
            process.exit();
        }
    }

    var min = require('../lib/minify');

    //example: /Users/ryan/Work/Projects/diff/thecoast/public_html/styles/mobile/mobile.css


    if(build_path){
        
        var admin_dest = build_path + "mobile.css";
        var public_dest = build_path + "mobile.min.css";
        var pub_path = build_path;

    } else {
        var pub_path =  publication_dir + pub_name + '/' + mobile_dir;
        var admin_dest = pub_path + "mobile.css";
        var public_dest;

        if(admin_only){
            public_dest = null;
        } else {
            public_dest = pub_path + "mobile.min.css";
        }

    }




    async.waterfall([
        min.create_temp_structure.bind(null, core_path, pub_path),
        min.generate_css,
        min.file_writer.bind(null, admin_dest, public_dest)
    ], function (err, result) {
        if(!err){
            console.log('\nStyles built for '+pub_name+' :\n');
            console.log([admin_dest,public_dest].join('\n'));
        } else {
            if(err.line && err.message && err.filename){
                console.log('Less Build Error ..............\n');
                console.log('  '+err.line+': '+err.message);
                console.log('in: '+err.filename);
            } else {
                console.log('ERROR: '+err);
            }
        }
        console.log('\n');
    });
}



if(is_setup){
    var custom_path = args.d || false;
    var pub_name = args._[1];

    // validate 

    if(!custom_path){
        if(typeof pub_name == "undefined"){
            console.log('\nERROR: Must specify publication.\n');
            process.exit();
        }
        if(!validate_pubname(pub_name)){
            console.log('\nERROR: Pub "'+pub_name+'" not found.\nin: '+publication_dir+'\n');
            process.exit();
        }
    }

    if(args.s || args.source){
        var source_name = args.s || args.source;
        if(typeof source_name == "undefined"){
            console.log('\nERROR: Must specify publication.\n');
            process.exit();
        }
        if(!validate_pubname(source_name)){
            console.log('\nERROR: Pub "'+source_name+'" not found.\nin: '+publication_dir+'\n');
            process.exit();
        }

        var theme_source =  publication_dir + source_name + '/' + mobile_dir + "theme.less";
    } else {
        var theme_source =  core_path + mobile_dir + "theme.default.less";
    }

    if(!custom_path){
        var theme_dest =  publication_dir + pub_name + '/' + mobile_dir + "theme.less";
        var custom_dest =  publication_dir + pub_name + '/' + mobile_dir + "custom.css";
    } else {
        var theme_dest =  custom_path + "theme.less";
        var custom_dest =  custom_path + "custom.css";
    }

    fs.readFile(theme_source, {encoding:'utf8'}, function(err, data){
        if(!err){
            fs.writeFile(theme_dest, data, {flag:"wx"}, function(err){
                if(!err){
                    console.log('\ntheme.less succefully added:\nat: '+theme_dest+'\n');
                } else if (err.code == 'EEXIST') {
                    console.log('theme.less already exists.');
                } else {
                    console.log('ERROR: ', err);
                }
            });
        } else {
            console.log('ERROR: ', err);
        }
    });

    fs.writeFile(custom_dest, "", {flag:"wx"}, function(err){
        if(!err){
            console.log('\ncustom.css succefully added:\nat: '+custom_dest+'\n');
        } else if (err.code == 'EEXIST') {
            console.log('custom.css already exists.');
        } else {
            console.log('ERROR: ', err);
        }
    })
}



if(is_theme){
    if(is_status){
        if(!args.p){
            var pub_theme, pub_file;
            theme_tools = require('../lib/theme');

            var pubs = get_child_dirs(publication_dir);
            var core_theme = core_path + "public_html/styles/mobile/theme.default.less";

            fs.readFile(core_theme, 'utf8', function(err, data){
                var core_file = data.split('\n');

                console.log('');
                for(var i = 0; i < pubs.length; i++){
                    pub_theme = publication_dir + pubs[i]+"/public_html/styles/mobile/theme.less";
                    try {
                        pub_file = fs.readFileSync(pub_theme, 'utf8').split('\n');
                        var missing = theme_tools.missing_vars(core_file, pub_file);
                        var extra = theme_tools.extra_vars(core_file, pub_file);
                        var spacing = Array(29 - pubs[i].length).join(".");
                        var status = missing.length || extra.length ? '..\u2716' : '\u2713';

                        console.log(pubs[i]+' '+spacing+status);

                        if(missing.length > 0){
                            console.log('  + '+missing.join(', '));
                        } 
                        if(extra.length > 0){
                            console.log('  - '+extra.join(', '));
                        } 
                    }
                    catch(err) {
                        // todo check error type
                        var spacing = Array(29 - pubs[i].length).join(".");
                        console.log(pubs[i]+spacing+' NO THEME FILE');
                    }
                }
                console.log('');
            });

        } else {
            // validate 
            var pub_name = args.p;
            if(!validate_pubname(pub_name)){
                console.log('\nERROR: Pub "'+pub_name+'" not found.\nin: '+publication_dir+'\n');
                process.exit();
            }


            t = require('../lib/theme');

            pub_theme = publication_dir + pub_name + "/public_html/styles/mobile/theme.less";
            default_theme = core_path + "public_html/styles/mobile/theme.default.less";


            pub_file = fs.readFileSync(pub_theme, 'utf8').split('\n');
            default_file = fs.readFileSync(default_theme, 'utf8').split('\n');

            var missing = t.missing_vars(default_file, pub_file);
            var extra = t.extra_vars(default_file, pub_file);

            if(missing.length || extra.length){
                console.log("\nChanges to "+pub_name+" theme file:");
                console.log(pub_theme + "\n");
                var changes = t.get_changes(default_file, pub_file);
                console.log(changes);
            } else {
                console.log('\n'+pub_name+ ' styles are up to date.\n');
            }
            console.log("\n");
        }

    } else if(is_upgrade){
        var pub_name = args.p;
        if(typeof pub_name == "undefined"){
            console.log('\nERROR: Must specify publication.\n');
            process.exit();
        }
        if(!validate_pubname(pub_name)){
            console.log('\nERROR: Pub "'+pub_name+'" not found.\nin: '+publication_dir+'\n');
            process.exit();
        }

        var theme_tools = require('../lib/theme');

        var pub_theme = publication_dir + pub_name + "/public_html/styles/mobile/theme.less";
        var default_theme = core_path + "public_html/styles/mobile/theme.default.less";

        var pub_file = fs.readFileSync(pub_theme, 'utf8').split('\n');
        var default_file = fs.readFileSync(default_theme, 'utf8').split('\n');

        var missing = theme_tools.missing_vars(default_file, pub_file);
        var extra = theme_tools.extra_vars(default_file, pub_file);

        if(missing.length || extra.length){
            console.log("\nChanges to "+pub_name+" theme file:");
            console.log(pub_theme + "\n");

            var changes = theme_tools.get_changes(default_file, pub_file);
            console.log(changes);

            var new_file = theme_tools.upgrade(default_file, pub_file);
            fs.writeFile(pub_theme, new_file.join('\n'), function (err) {
                if (err) throw err;
                console.log('Upgrade complete.\r');
            });
        } else {
            console.log('\n'+pub_name+ ' styles are up to date.\n');
        }

        console.log("\n");
    }
}
/*
var less = require('less');
less.render('@import "build.less";',{
    paths: [
        '/Users/ryan/Work/Projects/diff/ryan/public_html/styles/mobile/',
        '/Users/ryan/Work/Projects/diff/ryan/public_html/styles/mobile/less/'
    ],  
    filename: 'build.less', 
},
function (err, output) {
    console.log('LESS ERROR', err, output);
});

*/
